%include "lib.inc"
%define POINTER_SIZE 8
global find_word
section .text

;rdi принимает указатель на строку, а rsi - указатель на список
find_word:
    .next:
        add rsi, POINTER_SIZE
        push rdi
        push rsi
        call string_equals
        pop rsi
        pop rdi
        sub rsi, POINTER_SIZE
        test rax, rax
        jnz .return_word_pointer
        mov r11, [rsi]
        test r11, r11
        mov rsi, r11
        jnz .next
    .return_word_pointer:
        mov rax, rsi
        ret
