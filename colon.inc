%define next_element 0

%macro colon 2
    %ifid %2
        %2: dq next_element
    %else
        %error "it's not an id (arg 2)"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "it's not a string (arg 1)"
    %endif
    %define next_element %2
%endmacro
