global _start

%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define buffer_size 256

extern find_word

section .rodata
overflow_message: db "word is too long (buffer overflow)",10, 0
not_found_message: db "Word was not found",10, 0

section .bss
buffer: resb buffer_size


section .text

_start:
    .loop:
        mov rdi, buffer
        mov rsi, buffer_size
        call read_word         ;rax - word addr, rdx - word length
        .check_overflow:
            test rax, rax
            jz .overflow
        .check_word:
            mov rdi, rax        ;key addr
            mov rsi, next_element
            push rdx
            call find_word      ;returns in rax word addr, or 0 if was not found
            pop rdx
            test rax, rax
            je .not_found
        .continue:
            mov rdi, rax
            add rdi, 0x9        
            add rdi, rdx        ;move pointer to value
            call print_string
            call print_newline
            jmp .loop
        .overflow:
            mov rdi, overflow_message
            call print_error
            jmp .exit
        .not_found:
            mov rdi, not_found_message
            call print_error
            jmp .loop
        .exit:
            call print_newline
            xor rdi, rdi  
            call exit

